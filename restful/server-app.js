const express = require('express');
const CarpoolIntf = require('../carpool-intf');

const METHODS = (function() {
  var r = [];
  Object.getOwnPropertyNames(CarpoolIntf.prototype).forEach(m => {
    if (m == 'constructor') return;
    r.push(m);
  });
  return r;
})();

function bind(router, intf, methodName, options) {
  var {prefix, httpMethod} = options || {};
  prefix = prefix || '/';
  httpMethod = httpMethod || 'post';
  router[httpMethod](prefix + methodName, (req, res) => {
    intf[methodName](req.body, (e, r) => {
      if (e) res.status(500).send(e);
      else res.send(r);
    });
  });
}

function route(router, intf, prefix='/') {
  METHODS.forEach(m => {
    bind(router, intf, m, {prefix: prefix});
    bind(router, intf, m, {prefix: prefix, httpMethod: 'get'});
  });
}

function setupRestfulApp(app) {
  const bodyParser = require('body-parser');
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
}

function routeMock(router, prefix='/line-api-mock/') {
  const MockIntf = require('../direct/mock');
  route(router, new MockIntf(), prefix);
  routeApiIndex(router, prefix);
}

function routeApi(router, prefix='/line-api/') {
  // const ... = require('../direct/...');
  // route(router, new MockIntf());
}

function routeApiIndex(router, prefix='/web/line-api-mock/') {
  router.get(prefix, function (req, res) {
    res.set('Content-Type', 'text/html');
    var lines = [];
    lines.push('<html>');
    METHODS.forEach(n => {
      var url = prefix + n;
      lines.push(`<div><a href="${url}">${n}</a></div>`);
    });
    lines.push('</body>');
    res.send(lines.join(""));
  });
}

module.exports = {
  setupRestfulApp,
  routeMock,
  routeApi
};
