const url = require('url');
const CarpoolIntf = require('../carpool-intf');

function httpPost(targetUrl, data, cb) {
  var parsed = url.parse(targetUrl);
  var httpx = require(parsed.protocol.slice(0, -1));
  var options = {
    protocol: parsed.protocol,
    hostname: parsed.hostname,
    method: 'POST',
    path: parsed.path,
    headers: {
      'Content-Type': 'Content-Type: application/json',
      'Content-Length': Buffer.byteLength(data)
    }
  };
  if (parsed.port) options.port = parsed.port;
  var chunks = [];
  var req = httpx.request(options, (res) => {
    res.setEncoding('utf8');
    res.on('data', chunk => chunks.push(chunk));
    res.on('end', () => {
      var content = JSON.parse(chunks.join(''));
      if (res.statusCode != 200)
        cb(content);
      else
        cb(null, content);
    });
  });
  req.on('error', cb);
  req.write(data);
  req.end();
}

function getMethodUrl(apiUrl, method) {
  var r = apiUrl;
  if (!r.endsWith('/')) r += '/';
  r += method;
  return r;
}

function isString(x) {
  return (typeof x === 'string') || (x instanceof String);
}

class CarpoolClient extends CarpoolIntf {
  constructor(apiUrl) {
    super();
    this.apiUrl = apiUrl || 'http://localhost:3000';
  }
  post(method, data, cb) {
    httpPost(getMethodUrl(this.apiUrl, method), data, cb);
  }
}

(function buildMethods() {
  const METHODS = (function() {
    var r = [];
    Object.getOwnPropertyNames(CarpoolIntf.prototype).forEach(m => {
      if (m == 'constructor') return;
      r.push(m);
    });
    return r;
  })();
  METHODS.forEach(m => {
    CarpoolClient.prototype[m] = function(data, cb) {
      if (!isString(data)) data = JSON.stringify(data);
      this.post(m, data, cb);
    };
  });
})();

module.exports = CarpoolClient;
