const CarpoolIntf = require('../carpool-intf');

function ret(cb, result) {
  process.nextTick(() => cb(null, result));
}

class MockIntf extends CarpoolIntf {
  /* Purchase */
  call_me({phone_number, call_time}, cb) {
    ret(cb, {result: 'Success'});
  }
  purchaseonline_getyear({need}, cb) {
    ret(cb, [
      {id: '1', year: '2017'},
      {id: '2', year: '2018'}
    ]);
  }
  purchaseonline_getbrand({year}, cb) {
    ret(cb, [
      {id: '1', brand: 'Honda'},
      {id: '2', brand: 'Isuzu'}
    ]);
  }
  purchaseonline_getmodel({year, brand}, cb) {
    ret(cb, [
      {id: '1', model: 'Accord'},
      {id: '2', model: 'Brio'}
    ]);
  }
  purchaseonline_getcc({year, brand, model}, cb) {
    ret(cb, [
      {id: '1', cc: '1,800 CC'},
      {id: '2', cc: '2,000 CC'}
    ]);
  }
  purchaseonline_policy({year, brand, model, cc}, cb) {
    ret(cb, [
      {id: '1', policy: 'Muang Thai'}
    ]);
  }
  purchaseonline_quotation({year, brand, model, cc, quotationId}, cb) {
    ret(cb, [
      {filepath: 'file_path', filesize: '679.45KB'}
    ]);
  }
  purchaseonline_confirm(data, cb) {
    ret(cb, {result: 'Success'});
  }
  /* Me */
  my_bonus({id}, cb) {
    ret(cb, [
      {id: '1', detail: '1กก-8321'},
      {id: '2', detail: '4ง8302'}
    ]);
  }
  my_bonus_choose({vehicle_id}, cb) {
    var sample_detail = [
      '2010 Honda Accord',
      'Starting Carpool Bonus: 4,268.29 Baht',
      'Cost of claims: 768.29 Baht',
      'Carpool Bonus Remaining: 3,500.00 Baht' ];
    ret(cb, {detail: sample_detail.join('\n')});
  }
  my_policy({id}, cb) {
    ret(cb, [
      {id: '1', detail: '1กก-8321'},
      {id: '2', detail: '4ง8302'}
    ]);
  }
  my_policy_choose({vehicle_id}, cb) {
    ret(cb, {filepath: 'file_path', filesize: '679.45KB'});
  }
  my_profile_change_user({id, change_user}, cb) {
    ret(cb, {result: 'Success'});
  }
  my_profile_change_photo({id, filepath}, cb) {
    ret(cb, {result: 'Success'});
  }
  my_profile_change_address({id, change_address}, cb) {
    ret(cb, {result: 'Success'});
  }
  /* MyGroup */
  group_current({id}, cb) {
    ret(cb, [
      {group_id:'1', detail: '1กก-8321', group_name: 'group 1'},
      {group_id:'2', detail: '4ง8302',   group_name: 'group 2'}
    ]);
  }
  group_current_choose({group_id}, cb) {
    var sample_result = [
      'Group 1',
      'Remaining Carpool Bonus: 100%',
      'Group Carpool Bonus: 121,430.70',
      'Number of active members: 17',
      'Number of pending members: 3'
    ];
    ret(cb, {result: sample_result.join('\n')});
  }
  group_current_member({group_id, member_status}, cb) {
    var sample_result = [
      '1. Name Surname',
      '   Year, Brand, Model',
      '   Number of claims: 0',
      '   Carpool Bonus remaining: 100%',
      '   Joined as of: date',
      '',
      '2. Name Surname',
      '   Year, Brand, Model',
      '   Number of claims: 0',
      '   Carpool Bonus remaining: 100%',
      '   Joined as of: date...'
    ];
    ret(cb, {result: sample_result.join('\n')});
  }
  group_create({group_name, group_type, group_photo}, cb) {
    ret(cb, {result: 'Success'});
  }
  group_find({id}, cb) {
    ret(cb, [
      {group_id: '1', group_name: 'group 1'},
      {group_id: '2', group_name: 'group 2'}
    ]);
  }
  group_join_choose({id, group_id, vehicle_id}, cb) {
    ret(cb, {result: 'Success'});
  }
  /* Login */
  login({user, password}, cb) {
    ret(cb, {
      result: 'Success',
      id: 'idmember'
    });
  }
}

module.exports = MockIntf;
