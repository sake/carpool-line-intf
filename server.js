const express = require('express');
var { setupRestfulApp, routeMock, routeApi }  = require('./restful/server-app');
const router = express.Router();
routeMock(router);
routeMock(router, '/');
const app = express();
setupRestfulApp(app);
app.use(router);
app.listen(process.env.PORT || 3000);
