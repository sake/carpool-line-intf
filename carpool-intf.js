var provider = null;

class CarpoolIntf {
  static create() {
    return (global.provider == null)
      ? (new CarpoolIntf())
      : global.provider();
  }
  static registerProvider(provider) {
    global.provider = provider;
  }
  /* Purchase */
  call_me({phone_number, call_time}, cb) {}
  purchaseonline_getyear({need}, cb) {}
  purchaseonline_getbrand({year}, cb) {}
  purchaseonline_getmodel({year, brand}, cb) {}
  purchaseonline_getcc({year, brand, model}, cb) {}
  purchaseonline_policy({year, brand, model, cc}, cb) {}
  purchaseonline_quotation({year, brand, model, cc, quotationId}, cb) {}
  purchaseonline_confirm(data, cb) {}
  /* Me */
  my_bonus({id}, cb) {}
  my_bonus_choose({vehicle_id}, cb) {}
  my_policy({id}, cb) {}
  my_policy_choose({vehicle_id}, {}) {}
  my_profile_change_user({id, change_user}, cb) {}
  my_profile_change_photo({id, filepath}, cb) {}
  my_profile_change_address({id, change_address}, cb) {}
  /* MyGroup */
  group_current({id}, cb) {}
  group_current_choose({group_id}, cb) {}
  group_current_member({group_id, member_status}, cb) {}
  group_create({group_name, group_type, group_photo}, cb) {}
  group_find({id}, cb) {}
  group_join_choose({id, group_id, vehicle_id}, cb) {}
  /* Login */
  login({user, password}, cb) {}
}

module.exports = CarpoolIntf;
