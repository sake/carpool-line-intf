var CarpoolClient = require('./restful/client');
var MockInf = require('./direct/mock');

function run(subj, m) {
  var show = (e, r) => console.log(subj + ': ' + JSON.stringify(r));
  m.login({user: 'xxx', password: '3333'}, show);
  m.purchaseonline_getyear({need: 'yeardata'}, show);
  m.purchaseonline_getbrand({year: '2012'}, show);
};

run('mock', new MockInf);
run('https', new CarpoolClient('https://uat.carpool.co.th/web/line-api-mock'));
run('http', new CarpoolClient('http://localhost:3000'));
